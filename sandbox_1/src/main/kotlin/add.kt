package org.sandbox_1

/**
 * integer addition
 * @param a first arg
 * @param b second arg
 * return addition
 */
fun add(a: Int, b: Int): Int {
    return a + b
}

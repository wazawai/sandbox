package org.sandbox_1

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SandboxTest {

    @Test
    fun testAdd() {
        assertEquals(4, add(2, 2))
    }
}
